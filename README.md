# Pràctica codificació


## INDEX PRÀCTICA
- [Declaració de variables](#declaracio_variables)
- [Inicialització de text i llistes](#inicialitzacio)
- [Crear llista caràcters i repeticions](#llista_caracters)
- [Ordenar llista caràcters per aparicions al text](#ordenar_llista)
- [Compactar text](#compactar_text)
- [Càlcul bits i % de compactació del text](#calcul_bits)
- [Descompactar](#descompactar)


<div style="color:blue">

## Declaració de variables
</div>

He anat declarant les variables durant l'exercici.


## Inicialitzacio de text i llistes

He inicialitzat les llistes ficant un '\0' i un 0 respectivament a cada posició.


---

<div style="color:blue">

## Crear llista de caràcters i de repeticions
</div>

He demanat per pantalla el text, i he creat la llista de paraules, recorrent el text en busca de nous caràcters per a la llista.<br>
Quan trobi un caràcter que no hi sigui, el fica. Si ja hi és, passa al següent caràcter, fins al '\0'.


---

<div style="color:blue">

## Ordenar llista de caràcters per ordre d'aparicions al text
</div>

He utilitzat el mètode de la bombolla que es va projectar a classe per a ordenar els caràcters, amb una part del codi duplicada per a moure la llista de repeticions a la mateixa posició que la de caràcters.



    for(i=2; i<= ill; i++){
        for(j=0; j<= ill-i; j++){
            if(llistaRepe[j] < llistaRepe[j+1]){
                aux = llistaRepe[j];
                llistaRepe[j]= llistaRepe[j+1];
                llistaRepe[j+1]= aux;
                auxchar = llistaCar[j];
                llistaCar[j] = llistaCar[j+1];
                llistaCar[j+1]=auxchar;
            }
        }
    }



---

<div style="color:orange">

## Codificar text
</div>

Primer he recorregut la llista de caràcters buscant el primer caràcter del text, en quina posició és troba el primer caràcter. Llavors es passa la mateixa posició del vector codis cap al text compactat i s'augmenta una posició per a que surti del while i hi fiqui un dol·lar.


---

<div style="color:blue">

## Càlcul bits i % de compactació del text
</div>

He calculat els bits del text agafant l'index de text, que es la llargada del text ja que l'he recorregut durant l'exercici i l'he multiplicat per 8, que es quants bits ocupa cada caràcter.<br>
Després, he comptat quan ocupa el text codificat amb l'index del mateix text.<br>
He hagut de passar l'index del text compactat (itc) a float ja que l'operació per calcular el percentatge dona decimals en la majoria de casos i el programa no funcionava amb enters.<br>

---
<div style="color:blue">

## Descodificar 
</div>

He intentat agafar els caràcters fins arribar a un dòl·lar i comparar-los amb el de la llista de codi corresponent, però no trobo la manera. 


---





