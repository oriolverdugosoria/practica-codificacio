#include <stdio.h>
#include <stdlib.h>
#define BB while (getchar()!='\n')
#define MAX_TEXT 50
#define MAX_TEXT2 500
#define MAX_CARACTERS 50
#define MAX_REPETICIONS 50
#define MAX_CODI 50
#define MAX_DIGITS 4

int main()
{
    char text[MAX_TEXT+1];
    char textCompactat[MAX_TEXT2+1];
    char textDescompactat[MAX_TEXT2+1];
    char llistaCar[MAX_CARACTERS+1];
    int llistaRepe[MAX_REPETICIONS];
    char codis[MAX_CODI+1][MAX_DIGITS+1]={"0","1","00","01","10","11","000","001","010","011","100","101","110","111","0000","0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100","1101","1110","1111"};
    char codiAssignat[MAX_DIGITS+1];
    char codi[MAX_DIGITS+1];
    int it, itc, ic, ill, i, j;
    char aux, auxchar;
    int bitstext;
    float bits;
    float perc;
    float itcfloat;

    //inicialitzar text i llistes
    text[0]='\0';
    i=0;
    while(i<=MAX_CARACTERS){
        llistaCar[i]='\0';
        i++;
    }
    i=0;
    while(i<MAX_CARACTERS){
        llistaRepe[i]=0;
        i++;
    }
    //sol�licitar text per pantalla
    printf("Intro text:");
    scanf("%20[^\n]", text); BB;


    it=0;
    while(text[it]!='\0'){
        //trobar caracters per la llistaCar
        ill=0;
        while(text[it]!=llistaCar[ill] &&
              llistaCar[ill]!='\0')ill++;
        //si no l'has trobat?
        if(llistaCar[ill]=='\0'){
            llistaCar[ill]=text[it];
        }else{
              llistaRepe[ill]++;
        }
        it++;
    }
    it=0;
    while(llistaCar[ill]!='\0')ill++;
    it=0;
    while(it<ill){
        printf("\nEl caracter %c te %d repeticions", llistaCar[it], llistaRepe[it]);
        it++;
    }

    //Ordenar caracters per aparicions
    //M�tode de la bombolla
    ill=0;
    while(llistaCar[ill]!='\0')ill++;

    for(i=2; i<= ill; i++){
        for(j=0; j<= ill-i; j++){
            if(llistaRepe[j] < llistaRepe[j+1]){
                aux = llistaRepe[j];
                llistaRepe[j]= llistaRepe[j+1];
                llistaRepe[j+1]= aux;
                auxchar = llistaCar[j];
                llistaCar[j] = llistaCar[j+1];
                llistaCar[j+1]=auxchar;
            }
        }
    }

    it=0;
    printf("\n\nOrdre repeticio:");
    while(it<ill){
        printf("\ncaracter %c te %d repeticions", llistaCar[it], llistaRepe[it]);
        it++;
    }

    //convertir text a text compactat
    it=0;itc=0;
    while(text[it]!='\0'){
        i=0;
        while(llistaCar[i]!=text[it]){
            i++;
        }
        j=0;
        while(codis[i][j]!='\0'){
            textCompactat[itc]=codis[i][j];
            j++; itc++;
        }
        textCompactat[itc]='$';
        itc++;
        it++;
    }
    //per a que no surtin simbols aleatoris ni el dolar a l'ultim caracter del text compactat
    textCompactat[itc]='\0';
    textCompactat[itc-1]='\0';
    //
    itc--;
    printf("\n\nText compactat:\n\t%s", textCompactat);

    //printfs
    bitstext=it*8;
    printf("\n\nBits del text: %d*8 = %d bits", it, bitstext);

    printf("\nBits del text compactat:%d bits", itc);

    //crear float per a fer operaci� entre floats per a que el percentatge sigui exacte
    itcfloat=itc;
    bits=bitstext;
    perc=((bits-itcfloat)/bits)*100;
    printf("\n%% de compactacio: %.3f %%", perc);


    printf("\nCaracter\t repeticions \t codi assignat");
    it=0;
    while(it<ill){
        printf("\n%c\t\t%d\t\t %s", llistaCar[it], llistaRepe[it], codis[it]);
        it++;
    }
    //descompactar

    printf("\nIntrodueix el text compactat:");
    scanf("%500[^\n]", textCompactat);

    itc=0;
    while(textCompactat[itc]!='\0'){
        while(textCompactat[itc]=='$') itc++;
        it=0;
        while(textCompactat[itc]!='$' &&
              textCompactat[itc]!='\0'){
                codi[it]=textCompactat[itc];
                it++;itc++;
              }

    }
    return 0;
}

